import createCachedSelector from 're-reselect'

export const getNodes = state => state.nodes
export const getRootNodeId = state => state.rootNodeId
export const getNodesInNode = state => state.nodesInNode
export const getEditingNodeId = state => state.editing.id
export const getTopLayer = state => state.topLayer
export const getNode = (state, id) => getNodes(state)[id]
export const getChildIdList = (state, id) => {
  console.log({ nodesInNode: getNodesInNode(state) })
  console.log({ id })
  return getNodesInNode(state)[id] || []
}

export const getParentId = (state, id) => {
  const node = getNodes(state)[id]
  return node.parent
}

export const getSiblingIdList = (state, id) => {
  const topLayer = getTopLayer(state)
  const parentId = getParentId(state, id)
  const nodesInNode = getNodesInNode(state)

  return parentId ? nodesInNode[parentId] : topLayer
}

export const getIsInTopLayer = (state, id) => {
  const list = getTopLayer(state)
  return list.includes(id)
}

export const getChildNodeList = (state, id) => {
  const ids = getChildIdList(state, id)
  const nodes = getNodes(state)
  console.log({ ids })
  console.log({ nodes })
  return ids.map(id => nodes[id])
}

export const getTopLayerNodes = state => {
  const nodes = getNodes(state)
  const topLayer = getTopLayer(state)
  return topLayer.map(id => nodes[id])
}

// export const getTopLayerNodes = createCachedSelector(
//   getNodes,
//   getTopLayer,
//   (nodes, topLayer) => {
//     console.log({ nodes })
//     console.log({ topLayer })
//     return topLayer.map(id => nodes[id])
//   }
// )(state => state)
