import initialState from './initial'
import { ADD_NODE, SHIFT_NODE_RIGHT, SHIFT_NODE_LEFT } from '../../constants'

export default function(state = initialState.nodesInNode, action) {
  switch (action.type) {
    case ADD_NODE: {
      const parentId = action.parent
      const id = action.id

      return parentId
        ? {
            ...state,
            [action.id]: [],
            [parentId]: [...state[parentId], id]
          }
        : {
            ...state,
            [action.id]: []
          }
    }

    case SHIFT_NODE_RIGHT: {
      const { siblingIdList, id } = action
      const index = siblingIdList.indexOf(id)
      const preSiblingId = siblingIdList[index - 1]
      const newParentId = preSiblingId
      const childIdList = state[newParentId]

      if (!preSiblingId || childIdList.includes(id)) return state

      return {
        ...state,
        [newParentId]: [...state[newParentId], id]
      }
    }

    case SHIFT_NODE_LEFT: {
      const { siblingIdList, id } = action
      const index = siblingIdList.indexOf(id)
      const preSiblingId = siblingIdList[index - 1]
      const newParentId = preSiblingId

      if (!preSiblingId) return state

      return {
        ...state,
        [newParentId]: [...state[newParentId], id]
      }
    }

    default:
      return state
  }
}
