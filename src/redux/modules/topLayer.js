import initialState from './initial'
import { ADD_NODE, SHIFT_NODE_RIGHT } from '../../constants'

export default function(state = initialState.topLayer, action) {
  switch (action.type) {
    case ADD_NODE:
      const parentId = action.parent
      if (parentId) return state

      const arr = state.slice()
      const index = arr.indexOf(action.preSiblingId)
      arr.splice(index + 1, 0, action.id)
      return arr
    case SHIFT_NODE_RIGHT: {
      return state.filter(id => id !== action.id)
    }
    default:
      return state
  }
}
