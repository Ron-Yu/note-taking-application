import initialState from './initial'
import { ADD_NODE } from '../../constants'

export default function(state = initialState.editing, action) {
  switch (action.type) {
    case ADD_NODE:
      return {
        id: action.id
      }
    default:
      return state
  }
}
