import initialState from './initial'

export default function(state = initialState.rootNodeId, action) {
  switch (action.type) {
    default:
      return state
  }
}
