import uuid from 'uuid/v4'

import initialState from './initial'
import {
  EDIT_NODE,
  ADD_NODE,
  SHIFT_NODE_LEFT,
  SHIFT_NODE_RIGHT,
  ENTER,
  Tab
} from '../../constants'

export default function(state = initialState.nodes, action) {
  switch (action.type) {
    case ADD_NODE:
      return {
        ...state,
        [action.id]: {
          id: action.id,
          content: '',
          parent: action.parent
        }
      }
    case EDIT_NODE:
      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          content: action.content
        }
      }
    case SHIFT_NODE_RIGHT: {
      const { siblingIdList, id } = action
      const index = siblingIdList.indexOf(id)
      const preSiblingId = siblingIdList[index - 1]
      const newParentId = preSiblingId

      if (!preSiblingId) return state

      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          parent: newParentId
        }
      }
    }
    default:
      return state
  }
}

export const handleEditNode = (e, id) => {
  const content = e.target.value
  return {
    type: EDIT_NODE,
    content,
    id
  }
}

export const handelTabShift = (e, props) => {
  e.preventDefault()
  const type = e.shiftKey ? SHIFT_NODE_LEFT : SHIFT_NODE_RIGHT

  return {
    type,
    ...props
  }
}

export const handleAddNode = (e, props) => {
  e.preventDefault()
  return {
    type: ADD_NODE,
    id: uuid(),
    parent: props.parent,
    preSiblingId: props.id
  }
}

export const handleKeyDown = (e, props) => {
  switch (e.key) {
    case ENTER:
      return handleAddNode(e, props)
    case Tab:
      return handelTabShift(e, props)
    default:
      return { type: 'unknown' }
  }
}
