import uuid from 'uuid/v4'

export const initialId = uuid()

export default {
  nodes: {
    [initialId]: {
      id: initialId,
      content: '',
      parent: null
    }
  },
  nodesInNode: {
    [initialId]: []
  },
  topLayer: [
    initialId
  ],
  editing: {
    id: initialId
  },
  rootNodeId: null
}
