import { combineReducers } from 'redux'
import nodes from './nodes'
import nodesInNode from './nodesInNode'
import editing from './editing'
import rootNodeId from './rootNodeId'
import topLayer from './topLayer'

export default combineReducers({
  nodes,
  nodesInNode,
  topLayer,
  editing,
  rootNodeId,
})
