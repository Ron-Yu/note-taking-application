import React from 'react'
import Editor from './components/Editor'

import styles from './App.module.scss'

const App = () => {

  return (
    <div className={styles.root}>
      <Editor />
    </div>
  )
}

export default App
