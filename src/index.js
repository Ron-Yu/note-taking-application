import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import App from './App'
import store from './redux/store'

import styles from './index.module.scss'

function Root() {
  return (
    <div className={styles.root}>
      <Provider store={store}>
        <App />
      </Provider>
    </div>
  )
}

const rootElement = document.getElementById('root')
ReactDOM.render(<Root />, rootElement)
