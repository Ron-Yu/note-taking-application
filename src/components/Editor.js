import React from 'react'

import InitialNode from './InitialNode'
import styles from './Editor.module.scss'

const Editor = () => {
  return (
    <div className={styles.root}>
      <InitialNode />
    </div>
  )
}

export default Editor
