import React from 'react'
import { connect } from 'react-redux'

import { getTopLayerNodes } from '../redux/selectors'
import Node from './Node'

const InitialNode = props => {
  const { topLayerNodes } = props
  return (
    <>
      {topLayerNodes.map(node => (
        <Node key={node.id} {...node} />
      ))}
    </>
  )
}

const mapStateToProps = state => {
  return {
    topLayerNodes: getTopLayerNodes(state)
  }
}
export default connect(mapStateToProps)(InitialNode)
