import React, { useCallback, useRef, useEffect } from 'react'
import { connect } from 'react-redux'
import ContentEditable from 'react-contenteditable'

import {
  getEditingNodeId,
  getNode,
  getChildNodeList,
  getSiblingIdList
} from '../redux/selectors'
import { handleEditNode, handleKeyDown } from '../redux/modules/nodes'
import styles from './Node.module.scss'

const Node = props => {
  const {
    content,
    handleEditNode,
    handleKeyDown,
    id,
    isFocused,
    children
  } = props
  const editEle = useRef(null)

  useEffect(() => {
    if (isFocused) {
      editEle.current.focus()
    }
  }, [isFocused])

  const memoizedHandleEditNode = useCallback(
    e => {
      handleEditNode(e, id)
    },
    [handleEditNode, id]
  )

  const memoizedHandleKeyDown = useCallback(
    e => {
      console.log({ props })
      handleKeyDown(e, props)
    },
    [handleKeyDown, props]
  )

  return (
    <div className={styles.root}>
      <div className={styles.anchorPoint} />
      <ContentEditable
        innerRef={editEle}
        className={styles.node}
        onChange={memoizedHandleEditNode}
        onKeyDown={memoizedHandleKeyDown}
        onBlur={() => console.log('yaya')}
        html={content}
      />
      <div className={styles.childNodeList}>{children}</div>
      {/* <div
        className={styles.node}
        contenteditable="true"
        onInput={memoizedHandleEditNode}
        // onKeyDown={memoizedHandleKeyDown}
      >
        {content}
      </div> */}
    </div>
  )
}

const mapDispatchToProps = {
  handleEditNode,
  handleKeyDown
}

const mapStateToProps = (state, props) => {
  const id = props.id
  const editingNodeId = getEditingNodeId(state)
  const siblingIdList = getSiblingIdList(state, id)
  const node = getNode(state, id)

  const childrenNodeList = getChildNodeList(state, id)

  const childNodes = childrenNodeList.map(child => (
    <ConnectedNode key={child.id} parent={id} {...child} />
  ))

  return {
    ...node,
    isFocused: id === editingNodeId,
    children: childNodes,
    siblingIdList
  }
}

const ConnectedNode = connect(
  mapStateToProps,
  mapDispatchToProps
)(Node)

export default ConnectedNode
