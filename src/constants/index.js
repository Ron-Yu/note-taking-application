export const ADD_NODE = 'ADD_NODE'
export const EDIT_NODE = 'EDIT_NODE'
export const SHIFT_NODE_LEFT = 'SHIFT_NODE_LEFT'
export const SHIFT_NODE_RIGHT = 'SHIFT_NODE_RIGHT'


export const ENTER = 'Enter'
export const Tab = 'Tab'
